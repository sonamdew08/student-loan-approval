const { Pool } = require('pg');
const dotenv = require('dotenv');
var jwt = require('jsonwebtoken');
dotenv.config();
const pool = new Pool({
    user: process.env.user,
    host: process.env.host, 
    database: process.env.database,
    password: process.env.password,
    port: process.env.port
})

// pool.query('SELECT NOW()', (err, res) => {
//     console.log(err, res)
//     pool.end()
// });

const createUser = (request, response) => {
    const name = request.body.name
    const email = request.body.email
    const password = request.body.password;
    const currentDate = new Date()
    pool.query('INSERT into employee(name, email, password, created_on) VALUES ($1, $2, $3, $4)', [name, email, password, [currentDate]], (error, results) => {
        if (error) {
          console.log(error)
        }
        response.send(`User added: ${results}`)
    });
}

const loginUser = (request, response) => {
    const email = request.body.email
    const password = request.body.password
    pool.query('SELECT * from employee where email = $1 and password = $2', [email, password], (error, result) => {
        if(error){
            console.log(error)
        }
        if(result.rows.length === 0){
            response.send({auth: false})
        }
        else{
            var token = jwt.sign({id: result.rows[0].email}, process.env.JWT_PRIVATE_KEY, {expiresIn:  "12h"})
            console.log(token)
            response.send({auth: true, result: result, token: token})
        }
    })
}

const displayStudentDetail = (request, response) => {
    pool.query("SELECT users.user_id, users.name, users.phone, status.loan_amount, status.loan_status FROM public.user users INNER JOIN public.loan_status status on users.user_id = status.user_id where status.loan_status is not null", 
        (error, result) => {
            if(error){
                console.log(error)
            }
            response.send(result)
        })
}

const PersonalDetail = (request, response) => {
    const id = parseInt(request.params.userinfoid)
    console.log(id)
    pool.query("SELECT * FROM public.user_info where userinfo_id= $1", 
            [id], (error, result) => {
                if(error){
                    console.log(error)
                }
                response.send(result)
            })
}

const AddressDetail = (request, response) => {
    const id = parseInt(request.params.userinfoid)
    pool.query("SELECT address_id, userinfo_id, address, city, state, pincode, type FROM public.address_mapper where userinfo_id=$1", [id],
    (error, result) => {
        if(error){
            console.log(error)
        }
        response.send(result)
    })
}

const studentSchoolDetail = (request, response) => {
    const id = parseInt(request.params.id)
    pool.query("SELECT id, user_id, schoolname FROM public.student_schooldetails where user_id = $1", [id],
    (error, result) => {
        if(error){
            console.log(error)
        }
        response.send(result)
    })
}

const studentCourseDetail = (request, response) => {
    const id = parseInt(request.params.id)
    pool.query("SELECT id, user_id, course FROM public.course_details where user_id = $1", [id],
    (error, result) => {
        if(error){
            console.log(error)
        }
        response.send(result)
    })
}
const DocumentDetail = (request, response) => {
    const id = parseInt(request.params.userinfoid)
    pool.query("SELECT userinfo_id, document_type, document_id, documentimage_link, mapper_id, documentprooftype FROM public.document_mapper where userinfo_id = $1", [id],
    (error, result) => {
        if(error){
            console.log(error)
        }
        response.send(result)
    })
}

const studentGuardianRelationDetail = (request, response) => {
    const id = parseInt(request.params.id)
    console.log(id)
    pool.query("SELECT user_mapper.userinfo_id, user_mapper.user_relation_id, user_mapper.relation_type FROM public.user_mapper user_mapper, public.user_info user_info, public.user users WHERE user_info.userinfo_id = user_mapper.userinfo_id AND users.user_id = user_info.user_id AND users.user_id = $1", [id],
    (error, result) => {
        if(error){
            console.log("error")
            console.log(error)
        }
        response.send(result)
    })
}

const getCoApplicantEmploymentDetail = (request, response) => {
    const id = parseInt(request.params.userinfoid)
    console.log(id)
    pool.query("Select * from public.guardian_employment_details where user_info_id = $1", [id],
    (error, result) => {
        if(error){
            console.log("error")
            console.log(error)
        }
        response.send(result)
    })
}

function updateQuery(id, cols){
    var query=['UPDATE public.user_info'];
    query.push('SET');
    var set = [];
    Object.keys(cols).forEach(function (key, i) {
        set.push(key + ' = ($' + (i + 1) + ')'); 
    });
    query.push(set.join(', '));
    query.push('WHERE userinfo_id = ' + id);
    
    return query.join(' ');
}

const updatePersonalDetail = (request, response) => {
    console.log("hello");
    const id = parseInt(request.params.userinfoid)
    const body = JSON.stringify(request.body)
    console.log(id)
    console.log(body)
    var query = updateQuery(id, request.body);
    console.log("query", query);
    var colValues = Object.keys(request.body).map(function (key) {
        return request.body[key];
    });
    console.log("values", colValues);
    pool.query(query, colValues, function(err, result) {
        if (err) {
            console.log(err)
        }
        response.send(result);
    });
}

const getCityStateDetail = (request, response) => {
    const code = parseInt(request.params.pincode)
    pool.query("SELECT pincode, taluk, city, state FROM public.pincode_master where pincode_master.pincode = $1 LIMIT 1", [code],
    (error, result) => {
        if(error){
            console.log("error")
            console.log(error)
        }
        response.send(result)
    })
}

module.exports = {
    createUser,
    loginUser,
    displayStudentDetail,
    PersonalDetail,
    AddressDetail,
    studentSchoolDetail,
    studentCourseDetail,
    DocumentDetail,
    studentGuardianRelationDetail,
    getCoApplicantEmploymentDetail,
    updatePersonalDetail,
    getCityStateDetail
};