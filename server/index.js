const express = require('express')
const bodyParser = require('body-parser')
const db = require('./Database/Database')
const cors = require('cors')
const app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true,
}))
app.use(cors());

app.post('/signup', db.createUser);
app.post('/login', db.loginUser);
app.get('/userDetail', db.displayStudentDetail);
app.get('/userDetail/:userinfoid', db.PersonalDetail);
app.get('/userDetail/address/:userinfoid', db.AddressDetail);
app.get('/userDetail/school/:id', db.studentSchoolDetail);
app.get('/userDetail/course/:id', db.studentCourseDetail);
app.get('/userDetail/document/:userinfoid', db.DocumentDetail);
app.get('/userGuardianDetail/:id', db.studentGuardianRelationDetail);
app.get('/coApplicant/:userinfoid', db.getCoApplicantEmploymentDetail);
app.put('/userDetail/:userinfoid', db.updatePersonalDetail);
app.get('/addressDetail/:pincode', db.getCityStateDetail);
app.listen(3001, () =>
  console.log('Express server is running on localhost:3001')
);
