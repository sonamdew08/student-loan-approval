import React from "react";
import { Input, Form, FormGroup, Label, Col, Button, Alert, UncontrolledAlert } from 'reactstrap';
import "../../../App.css";
import DatePicker from "react-datepicker"; 
import "react-datepicker/dist/react-datepicker.css";
import * as moment from 'moment';


var visible;

class PersonalDetail extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            date_of_birth: new Date(moment(this.props.personalDetail.date_of_birth))
        }
        this.handleChange = this.handleChange.bind(this);
        this.saveChanges = this.saveChanges.bind(this);
        // this.getDateOfBirth = this.getDateOfBirth.bind(this);
    }

    handleChange(event){
        this.setState({[event.target.name]: event.target.value}, () => {
            console.log(this.state);
        })
    }

    getDateOfBirth(date){
        this.setState({
            date_of_birth: date
        }, () => console.log(this.state.date_of_birth))
    }

    saveChanges(id){
        this.props.sendUpdateDetail(this.state, id);
        visible = this.props.editable;        
    }
    
    render(){
        const { personalDetail, editable } = this.props
        
        var dob = new Date(personalDetail.date_of_birth)
        console.log(dob.toLocaleDateString())
        return (
            <div>
                { visible && <UncontrolledAlert >Details successfully updated</UncontrolledAlert>}
                <p className="detail-header">
                    <p className="float-left ">Personal Detail </p>
                    { editable && <Button className="float-right" color="success" onClick={() => this.saveChanges(personalDetail.userinfo_id)}>Save</Button>}
                </p>
                <div className="clearfix"></div>
                
                <div className='personaldetail m-10'>
                    <Form>
                        <FormGroup row>
                            <Label sm={2}>Name</Label>
                            <Col sm={4}>
                                <Input type="text" defaultValue={personalDetail.name} name="name" onChange={this.handleChange} readOnly={!editable}/>
                            </Col>
                            <Label sm={2}>Email Address</Label>
                            <Col sm={4}>
                                <Input type="text" defaultValue={personalDetail.email} name="email" onChange={this.handleChange} readOnly={!editable}/>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={2}>Father Name</Label>
                            <Col sm={4}>
                                <Input type="text" defaultValue={personalDetail.father_name} name="father_name" onChange={this.handleChange} readOnly={!editable}/>
                            </Col>
                            <Label sm={2}>Mother Name</Label>
                            <Col sm={4}>
                                <Input type="text" defaultValue={personalDetail.mother_name} name="mother_name" onChange={this.handleChange} readOnly={!editable}/>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={2}>Contact No</Label>
                            <Col sm={4}>
                                <Input type="text" defaultValue={personalDetail.phonenumber} name="phonenumber" onChange={this.handleChange} readOnly={!editable}/>
                            </Col>
                            <Label sm={2}>Date of Birth</Label>

                            <Col sm={4}>

                            <DatePicker
                                    name="date_of_birth"
                                    onChange={date_of_birth=>this.setState({date_of_birth})} 
                                    dateFormat="DD-MM-YYYY"
                                    value={this.state.date_of_birth.toLocaleDateString()}
                                    readOnly={!editable}
                                />
                                {/* <Input type="text" value={this.state.date_of_birth.toLocaleDateString()} name="date_of_birth" onChange={this.handleChange} readOnly={!editable}/> */}
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={2}>Gender</Label>
                            <Col sm={4}>
                                <Input type="text" defaultValue={personalDetail.gender} name="gender" onChange={this.handleChange} readOnly={!editable}/>
                            </Col>
                            <Label sm={2}>Marital Status</Label>
                            <Col sm={4}>
                                <Input type="text" defaultValue={personalDetail.maritalstatus} name="maritalstatus" onChange={this.handleChange} readOnly={!editable}/>
                            </Col>
                        </FormGroup>
                    </Form>                    
                </div>
            </div>
        );        
    }
}

export default PersonalDetail;