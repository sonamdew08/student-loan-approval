import React from "react";
import { Form, FormGroup, Label, Col, Input, CustomInput } from "reactstrap";

class CourseDetail extends React.Component {
    render(){
        const { courseDetail, schoolDetail } = this.props
        return (
            <div>
                <p className='detail-header'>Course Detail</p>
                <div className='personaldetail'>
                    <Form>
                        <FormGroup row>
                            <Label sm={3}>University/College</Label>
                            <Col sm={9}>
                                <Input type="text" value={schoolDetail.schoolname} readOnly />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={3}>Name of the Course</Label>
                            <Col sm={9}>
                                <Input type="text" value={courseDetail.course} readOnly />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={3}>Course Duration</Label>
                            <Col sm={5}>
                                <Input type="text" /> <span>years</span>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={3}>Type of Course</Label>
                            <Col sm={9}>
                                <CustomInput type="radio" id="Medicine" name="courseType" label="Medicine" />
                                <CustomInput type="radio" id="Management" name="courseType" label="Management" />
                                <CustomInput type="radio" id="Engineering" name="courseType" label="Engineering" />
                                <CustomInput type="radio" id="CertificateCourse" name="courseType" label="Certificate Course" />
                                <CustomInput type="radio" id="Vocational Course" name="courseType" label="Vocational Course" />
                                <CustomInput type="radio" id="Others" name="courseType" label="Others" />          
                            </Col>
                        </FormGroup>
                    </Form>
                </div>
            </div>
        );
    }
}

export default CourseDetail;