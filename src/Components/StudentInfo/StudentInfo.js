import React from 'react';
import Header from '../LandingPage/Header';
import { TabContent, 
        TabPane, 
        Nav, 
        NavItem, 
        NavLink, 
        Row, Col } from 'reactstrap';
import StudentDetail from './StudentDetail/StudentDetail';
import GuardianDetail from './GuardianDetail/GuardianDetail';
import axios from 'axios';
import classnames from 'classnames';
import {store} from "../Store/Store";
import {removeJWTToken} from "../Action/Action";
import CoApplicantEmploymentDetail from './CoApplicantEmploymentDetail/CoApplicantEmploymentDetail';
import { connect } from 'react-redux';
import UnAuthenticate from '../UnAuthenticate/unauthenticate';


class StudentInfo extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            studentDetail: {
                id: "",
                personalDetail: [],
                addressDetail: [],
                document: [],
                schoolDetail: "",
                courseDetail: []
            },
            guardianDetail: {
                id: "",
                personalDetail: [],
                addressDetail: [],
                document: []                
            },
            employmentDetail: [],
            isEmploymentDetailFetched: false,
            user_id: this.props.location.state.user_id,
            fetch: false,
            activeTab: '1',
            updated: false
        }
        this.toggle = this.toggle.bind(this);
        this.updatePersonalDetail = this.updatePersonalDetail.bind(this);
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
          this.setState({
            activeTab: tab
          });
        }
    }

    async componentDidMount(){
        await this.getStudentGuardianDetail()        
    }

    getStudentGuardianDetail = () =>{
        axios.get(`http://localhost:3001/userGuardianDetail/${this.state.user_id}`)
        .then(response => {
            let studentDetailCopy = JSON.parse(JSON.stringify(this.state.studentDetail))
            let guardianDetailCopy = JSON.parse(JSON.stringify(this.state.guardianDetail))
            studentDetailCopy.id = response.data.rows[0].userinfo_id
            guardianDetailCopy.id = response.data.rows[0].user_relation_id
            this.setState({
                studentDetail: studentDetailCopy,
                guardianDetail: guardianDetailCopy
            }, () => {
                this.getStudentDetails()
                this.getGuardianDetail()
                this.getCoApplicantEmploymentDetail()
            })
        })
    }

    getCoApplicantEmploymentDetail = () => {
        axios.get(`http://localhost:3001/coApplicant/${this.state.guardianDetail.id}`)
        .then(response => {
            this.setState({employmentDetail: response.data.rows[0]}, () => {
                console.log(this.state.employmentDetail)
            })
        })
    }

    getStudentDetails = () => {
        console.log(this.state.studentDetail)
        axios.all([
            axios.get(`http://localhost:3001/userDetail/${this.state.studentDetail.id}`),
            axios.get(`http://localhost:3001/userDetail/address/${this.state.studentDetail.id}`),
            axios.get(`http://localhost:3001/userDetail/document/${this.state.studentDetail.id}`),
            axios.get(`http://localhost:3001/userDetail/school/${this.state.user_id}`),
            axios.get(`http://localhost:3001/userDetail/course/${this.state.user_id}`)
        ])
        .then(response => {
            let studentDetailCopy = JSON.parse(JSON.stringify(this.state.studentDetail))
            studentDetailCopy.personalDetail = response[0].data.rows[0]
            studentDetailCopy.addressDetail = response[1].data.rows
            studentDetailCopy.document = response[2].data.rows
            studentDetailCopy.schoolDetail = response[3].data.rows[0]
            studentDetailCopy.courseDetail = response[4].data.rows[0];
            this.setState({
                studentDetail: studentDetailCopy,
                fetch: true
            }, () => {
                console.log(this.state.studentDetail)
            })
        })
    }
    
    getGuardianDetail = () => {
        console.log(this.state.guardianDetail)
        axios.all([
            axios.get(`http://localhost:3001/userDetail/${this.state.guardianDetail.id}`),
            axios.get(`http://localhost:3001/userDetail/address/${this.state.guardianDetail.id}`),
            axios.get(`http://localhost:3001/userDetail/document/${this.state.guardianDetail.id}`)
        ])
        .then(response => {
            let guardianDetailCopy = JSON.parse(JSON.stringify(this.state.guardianDetail))
            guardianDetailCopy.personalDetail = response[0].data.rows[0]
            guardianDetailCopy.addressDetail = response[1].data.rows
            guardianDetailCopy.document = response[2].data.rows

            this.setState({
                guardianDetail: guardianDetailCopy,
                fetch: true
            }, () => {
                console.log(this.state.guardianDetail)
            })
        })
    }

    employeeDetailUpdate = () => {
        console.log("inside of employeeDetailUpdate")
        this.setState({isEmploymentDetailFetched: true},()=>{
            console.log(this.state.isEmploymentDetailFetched)
            this.getCoApplicantEmploymentDetail()
        })
    }

    handleLogout = () => {
        store.dispatch(removeJWTToken());
    }

    updatePersonalDetail(details, id){
        console.log("inside studentInfo", details)
        axios.put(`http://localhost:3001/userDetail/${id}`, details)
        .then(response => {
            console.log(response.data)
            this.setState({updated: true})
        })
    }

    getDocumentDetail = () => {
        console.log("student information")
    }

    render(){
        if(this.props.token){
        return (
            <div>
                <Header>
                    <div className='navbar'>
                    <a href='/login' className="btn btn-success float-right" onClick={this.handleLogout}>Logout</a>
                    </div>
                </Header>
                <div className='container'>
                    <h2>Student Detail</h2>
                    {this.state.fetch && <div className='student-info-container'>
                    <Nav tabs>
                        <NavItem>
                            <NavLink
                                className={classnames({ active: this.state.activeTab === '1' })}
                                onClick={() => { this.toggle('1'); }}
                                >
                                Student Detail
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                className={classnames({ active: this.state.activeTab === '2' })}
                                onClick={() => { this.toggle('2'); }}
                                >
                                Guardian/Co-Applicant Detail
                            </NavLink>
                        </NavItem>
                        {this.state.employmentDetail && <NavItem>
                            <NavLink
                                className={classnames({ active: this.state.activeTab === '3' })}
                                onClick={() => { this.toggle('3'); }}
                                >
                                Employment Detail
                            </NavLink>
                        </NavItem>
                    }
                    </Nav>
                    <TabContent activeTab={this.state.activeTab}>
                        <TabPane tabId="1">
                            <Row>
                                <Col sm="12">
                                    <StudentDetail detail={this.state.studentDetail} 
                                                editNewDetails = {this.updatePersonalDetail} 
                                                updated={this.state.updated}
                                                document={this.getDocumentDetail}/>
                                </Col>
                            </Row>  
                        </TabPane>
                    <TabPane tabId="2">
                        <Row>
                            <Col sm="12">
                                <GuardianDetail employmentDetail={this.state.employmentDetail} editNewDetails = {this.updatePersonalDetail} detail={this.state.guardianDetail} fetchEmploymentDetail={this.employeeDetailUpdate} />
                            </Col>
                        </Row>
                    </TabPane>
                    {this.state.employmentDetail && <TabPane tabId="3">
                        <Row>
                            <Col sm="12">
                                <CoApplicantEmploymentDetail employmentDetail={this.state.employmentDetail} />
                            </Col>
                        </Row>
                    </TabPane>}
                </TabContent>
                </div>}
                </div>   
            </div>
        );
        }
        else{
            return (
                <UnAuthenticate />
            );
        }
    }
}

function mapStateToProps(state){
    return { token: state.jwtToken }
}

export default connect(mapStateToProps)(StudentInfo);
