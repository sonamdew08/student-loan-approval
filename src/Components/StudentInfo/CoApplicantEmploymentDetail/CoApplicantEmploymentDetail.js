import React from "react";
import { Form, FormGroup, Label, Col, Input} from 'reactstrap';

class CoApplicantEmploymentDetail extends React.Component{
    render(){
        const {employmentDetail} = this.props
        console.log(employmentDetail);
        return (
            <div style={{paddingBottom: 20, marginTop: '1rem'}}>
                <p className='detail-header'>Employment Detail</p>
                <div className='personaldetail'>
                    <Form>
                        <FormGroup row>
                            <Label sm={3}>Occupation</Label>
                            <Col sm={9}>
                                <Input type="text" value={employmentDetail.occupation} />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={3}>Name of the employer/Business</Label>
                            <Col sm={9}>
                                <Input type="text"  value={employmentDetail.name_of_employer}/>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={3}>Address</Label>
                            <Col sm={9}>
                                <Input type="text" value={employmentDetail.address} />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={3}>City</Label>
                            <Col sm={9}>
                                <Input type="text" value={employmentDetail.city}/>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={3}>State</Label>
                            <Col sm={9}>
                                <Input type="text" value={employmentDetail.state}/>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={3}>PinCode</Label>
                            <Col sm={9}>
                                <Input type="text"  value={employmentDetail.pincode}/>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={3}>No of years in current Employment/Business</Label>
                            <Col sm={9}>
                                <Input type="text"  value={employmentDetail.num_of_years_current_employment}/>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={3}>Designation</Label>
                            <Col sm={9}>
                                <Input type="text" value={employmentDetail.designation}/>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={3}>Monthly salary</Label>
                            <Col sm={9}>
                                <Input type="text" value={employmentDetail.gross_monthly_salary}/>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={3}>Total Monthly Obligations Rs.</Label>
                            <Col sm={9}>
                                <Input type="text"  value={employmentDetail.total_monthly_obligations}/>
                            </Col>
                        </FormGroup>
                    </Form>                    
                </div>
            </div>
        );
    }
}

export default CoApplicantEmploymentDetail;