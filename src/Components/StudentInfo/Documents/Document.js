import React from "react";
import { Row, Col, Card, CardHeader, CardText, CardBody, Button } from 'reactstrap';
import { Modal, ModalBody, ModalFooter } from "reactstrap";
import { Form, FormGroup, Input, Label, CustomInput, FormFeedback } from "reactstrap";

class Document extends React.Component{
    
    state = {
        open: false
    }

    ontoggle = () => {
        this.setState(({ open }) => ({ open: !open }), () => {
            console.log("Modal", this.state.open)
        });
    }

    handleChange = (event) => {
        this.setState({[event.target.name]: event.target.value}, () => console.log(this.state))
    } 

    submitDocument = (id) => {
        const documentInfo = {
            documentprooftype: this.state.documentprooftype,
            documenttype: this.state.documenttype,
            file: this.state.file
        }
        console.log("document", id)
        this.props.documentdetail(documentInfo, id)
    }

    render(){
        const {documents, editable} = this.props
        return (
            <div style={{marginTop: '1rem'}}>
                <p className="detail-header">
                    <p className="float-left ">Document</p>
                    { editable && <Button className="float-right" color="success" onClick={this.ontoggle} >Upload Document</Button>}
                </p>
                <div className="clearfix"></div>
                <div className='personaldetail'>
                    <Row>
                        {Array.isArray(documents) && documents.map((document, index) => (
                            <div className="document_div">
                                <Col sm={6} key={index}>
                                    <Card>
                                        <CardHeader>
                                            <h6>{document.documentprooftype}</h6>
                                        </CardHeader>
                                        <CardBody>
                                            <CardText>{ document.document_type } { document.document_id }</CardText>
                                        </CardBody>                                    
                                    </Card>
                                </Col>
                            <Modal isOpen={this.state.open} size="lg">
                            <ModalBody>
                                <Form>
                                    <FormGroup row>
                                        <Label sm={6}>Document proof type</Label>
                                            <Col sm={6}>
                                                <CustomInput type="radio" id="document" name="documentprooftype" value="Document proof" label="Document proof"  onChange={this.handleChange}/>
                                                <CustomInput type="radio" id="address" name="documentprooftype" value="Address proof" label="Address proof" onChange={this.handleChange}/>
                                            </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Label sm={6}>Document type</Label>
                                            <Col sm={6}>
                                                <CustomInput type="radio" id="dl" name="documenttype" value="Driving license" label="Driving license"  onChange={this.handleChange}/>
                                                <CustomInput type="radio" id="adhaar" name="documenttype" value="Adhaar Card" label="Adhaar Card" onChange={this.handleChange}/>
                                                <CustomInput type="radio" id="passport" name="documenttype" value="Passport" label="Passport" onChange={this.handleChange}/>
                                            </Col>
                                    </FormGroup>
                                    <FormGroup className="files">
                                        <input type="file" name="file" onChange={this.handleChange}/>
                                    </FormGroup>
                                    <button type="button" class="btn btn-success btn-block" onClick={() => this.submitDocument(document.userinfo_id)}>Upload</button> 
                                </Form>
                            </ModalBody>
                            <ModalFooter>
                                <Button color="secondary" onClick={this.ontoggle}>Cancel</Button>
                            </ModalFooter>
                        </Modal>
                        </div>
                        ))}
                    </Row>               
                </div>
                
            </div>
        );        
    }
}

export default Document;