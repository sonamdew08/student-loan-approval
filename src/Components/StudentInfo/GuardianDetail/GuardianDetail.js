import React from 'react';
import PersonalDetail from "../PersonalDetail/PersonalDetail";
import AddressDetail from "../AddressDetail/AddressDetail";
import Document from "../Documents/Document";
import { Button } from "reactstrap";
// import CoApplicantDetail from "./Co-ApplicantEmploymentDetail/CoApplicantEmploymentDetail";
import { Modal, ModalBody, ModalFooter } from "reactstrap";
import { Form, FormGroup, Input, Label, Col, CustomInput, FormFeedback } from "reactstrap";
import axios from "axios";

class GuardianDetail extends React.Component{
    
    constructor(props){
        super(props);
        this.state = {
            open: false,
            occupation: "",
            employer_name: "",
            address: "",
            city: "",
            state: "",
            pincode: "",
            designation: "",
            monthly_salary: "",
            disableText: true,
            editable:false, 
            error:"",
            required: "Field is required"
        }
        this.getDetails = this.getDetails.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    editDetails = () => {
        
        this.setState({editable: !this.state.editable})
    }

    sendDetail = (details, id) => {
        console.log("inside studentDetail", details);
        this.props.editNewDetails(details, id);
    }
    
    getDetails(event){
        this.setState({[event.target.name]: event.target.value}, () => {
            if(this.state.occupation === "others"){
                this.setState({disableText: false}, () => console.log("input text"))
            }
            if(this.state.pincode.length === 6){
                axios.get(`http://localhost:3001/addressDetail/${this.state.pincode}`)
                .then(response => {
                    console.log(response.data.rows)
                    if(response.data.rows.length > 0){
                        this.setState({city: response.data.rows[0].city, state: response.data.rows[0].state, error:""},
                            () =>{
                                console.log(this.state)
                            }
                        )
                    }
                    else{
                        this.setState({error: "Enter correct pincode"})
                    }
                    
                })
            }
        });
    }

    handleSubmit(value){
        console.log(value)
        console.log("Hello from modal", this.state);
        console.log()
        const { occupation, 
            employer_name, 
            address, 
            city, 
            state, 
            pincode, 
            designation, 
            monthly_salary,
            no_of_years_in_employment,
            monthly_obligations } = this.state;
        const coApplicantDetail = {
            occupation: occupation,
            name_of_employer: employer_name,
            address: address,
            city: city,
            state: state,
            pincode: pincode,
            designation: designation,
            gross_monthly_salary: monthly_salary,
            num_of_years_current_employment: no_of_years_in_employment,
            total_monthly_obligations: monthly_obligations,
            user_info_id: value
        }
        console.log(coApplicantDetail)
        axios.post("http://localhost:8080/coApplicant", coApplicantDetail)
        .then(response => {
            console.log(response)
            //this.props.history.push('/login')
            this.setState(({ open }) => ({ open: !open }), () => {
                console.log("Modal", this.state.open);
                this.props.fetchEmploymentDetail();
            });
        })
    }
    
    ontoggle = () => {
        this.setState(({ open }) => ({ open: !open }), () => {
            console.log("Modal", this.state.open)
        });
    }

    render(){
        const {detail, employmentDetail} = this.props
        var disable = false;
        
        if(employmentDetail !== undefined){
            disable = true;
        }
        return (
            <div style={{paddingBottom: 20, marginTop: '1rem'}}>
                <PersonalDetail personalDetail = {detail.personalDetail} editable={this.state.editable} sendUpdateDetail = {this.sendDetail}/>
                <AddressDetail addressDetail = {detail.addressDetail}/>
                <Document documents = {detail.document}/>
                <div className='float-right mt-2'>
                    <Button color='success' onClick={this.ontoggle} className='mr-3' disabled={disable}>Add Co-Applicant Employment Detail</Button>
                    <Button color='success' onClick={this.editDetails}>Edit</Button>
                </div>
                <Modal isOpen={this.state.open} size="lg">
                    <Form>
                        <ModalBody>                        
                            <FormGroup row>
                                <Label sm={6}>Occupation</Label>
                                    <Col sm={6}>
                                        <CustomInput type="radio" id="Employed" name="occupation" value="Employed" label="Employed" onChange = {this.getDetails} />
                                        <CustomInput type="radio" id="SelfEmployed" name="occupation" value="Self Employed" label="Self Employed" onChange = {this.getDetails}/>
                                        <CustomInput type="radio" id="Retired" name="occupation" value="Retired" label="Retired" onChange = {this.getDetails}/>
                                        <CustomInput type="radio" id="Other" name="occupation" value="others" label="Others" onChange = {this.getDetails}/><input type="text" name="occupation" disabled={this.state.disableText} onChange = {this.getDetails}/>
                                    </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={6}>Name of the Employer/Business</Label>
                                    <Col sm={6}>
                                        <Input name="employer_name" 
                                            type="text" 
                                            onChange = {this.getDetails} 
                                            invalid = {this.state.employer_name ? true : false} 
                                         />
                                        { this.state.employer_name && <FormFeedback>{this.state.required}</FormFeedback>}
                                    </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={6}>Address of the Employer/Business</Label>
                                    <Col sm={6}>
                                        <Input name="address" type="text" onChange = {this.getDetails} required/>
                                    </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={6}>City</Label>
                                    <Col sm={6}>
                                        <Input name="city" type="text" value={this.state.city} onChange = {this.getDetails} readOnly/>
                                    </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={6}>State</Label>
                                    <Col sm={6}>
                                        <Input name="state" type="text" value={this.state.state} onChange = {this.getDetails} readOnly/>
                                    </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={6}>PIN Code</Label>
                                    <Col sm={6}>
                                        <Input  name="pincode" 
                                                type="text"
                                                onChange = {this.getDetails}
                                                invalid = {this.state.error ? true : false} required />
                                        { this.state.error && <FormFeedback>{this.state.error}</FormFeedback>}
                                    </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={6}>Designation/Profession</Label>
                                    <Col sm={6}>
                                        <Input name="designation" type="text" onChange = {this.getDetails} required/>
                                    </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={6}>No of years in current Employment/Business</Label>
                                    <Col sm={6}>
                                        <Input name="no_of_years_in_employment" type="text" onChange = {this.getDetails} required/>
                                    </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={6}>Gross Monthly Salary / Income Rs.</Label>
                                    <Col sm={6}>
                                        <Input name="monthly_salary" type="text" onChange = {this.getDetails} required/>
                                    </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={6}>Total Monthly Obligations Rs.</Label>
                                    <Col sm={6}>
                                        <Input name="monthly_obligations" type="text" onChange = {this.getDetails} required/>
                                    </Col>
                            </FormGroup>
                        </ModalBody>
                    </Form>
                    <ModalFooter>
                        <Button color="secondary" onClick={() => this.handleSubmit(detail.personalDetail.userinfo_id)}>Submit</Button>
                        <Button color="secondary" onClick={this.ontoggle}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default GuardianDetail;
