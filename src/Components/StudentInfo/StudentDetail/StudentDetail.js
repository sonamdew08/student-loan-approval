import React from 'react';
import PersonalDetail from "../PersonalDetail/PersonalDetail";
import AddressDetail from "../AddressDetail/AddressDetail";
import Document from "../Documents/Document";
import CourseDetail from "../CourseDetail/CourseDetail";
import { Button } from "reactstrap";

class StudentDetail extends React.Component{

    state = {
        editable:false
    }

    editDetails = () => {
        
        this.setState({editable: !this.state.editable})
    }

    sendDetail = (details, id) => {
        console.log("inside studentDetail", details);
        this.setState({editable: !this.state.editable}, () => {
            this.props.editNewDetails(details, id);
        })
        
    }

    documentDetail = (documentInfo, id) => {
        console.log("student detail", documentInfo, id)
        this.props.document()
    }
    
    render(){
        const {detail} = this.props
        
        console.log("studentDetail", detail)
        return (
            <div style={{paddingBottom: 20, marginTop: '1rem'}}>
                <PersonalDetail personalDetail = {detail.personalDetail} updated={this.props.updated} editable={this.state.editable} sendUpdateDetail = {this.sendDetail}/>
                {detail.courseDetail && detail.schoolDetail ? 
                    <CourseDetail courseDetail = {detail.courseDetail} schoolDetail = {detail.schoolDetail} />
                : ""}                
                <AddressDetail addressDetail = {detail.addressDetail} />
                <Document documents = {detail.document} editable={this.state.editable} documentdetail = {this.documentDetail}/>
                <div className='float-right mt-2'>
                    <Button color='success' onClick={this.editDetails}>Edit</Button>
                </div>                
            </div>
        );
    }
}

export default StudentDetail;
