import React from "react";
import { Input, Form, FormGroup, Label, Col } from 'reactstrap';

class AddressDetail extends React.Component{
                
    render(){
        const {addressDetail} = this.props
        console.log("Address component", this.props)
        return (
            <div style={{marginTop: '1rem'}}>
            <p className='detail-header'>Address Detail</p>
                <div className='personaldetail'>

                    {Array.isArray(addressDetail) && <Form>
                        {addressDetail.map((address, index) => {
                            let addressdata = `${address.address}, ${address.city}, ${address.state}` 
                            return (
                                <FormGroup row key={index}>
                                    {address.type === 'c' || address.type === 'cAsp'?
                                    <Label sm={2}>Current Address</Label>:
                                    <Label sm={2}>Parmanent Address</Label>
                                   } <Col sm={10}>
                                        <Input type="text" value={addressdata} readOnly />
                                    </Col>
                                </FormGroup>
                            )
                            })}
                    </Form>}
                </div>
            </div>
        );        
    }
}

export default AddressDetail;