import React from "react";
import {Container, Col, Card, CardBody} from "reactstrap";

class UnAuthenticate extends React.Component{
    render(){
        return (
            <Container className='login-form'>
                    <Col md="4">
                        <Card className='d-flex justify-content-center'>
                            <CardBody>
                                <p>401</p>
                                <p>Unauthenticate: Access to this resource in denied</p>
                                <Col>
                                    <a href="/login" className='btn btn-success' >Move to Login page</a>
                                </Col>
                            </CardBody>
                        </Card>
                    </Col>
                </Container>
        );
    }
}

export default UnAuthenticate;