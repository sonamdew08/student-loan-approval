export default function validateInputs(name, email, password, confirmPassword){
    const error = {};
    if(name.length === 0){
        error['name'] = "Name can't be empty";
    }    
    if(!email.split('@').includes('varthana.com')){
        error['email'] = "Not a valid mail Id";
    }
    if(password.length < 8 ){
        error['password'] = "Password must be 8 character long"
    }
    if(password !== confirmPassword){
        error['confirmPassword'] = "Password doesn't match"
    }
    return error;
}

