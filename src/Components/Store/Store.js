import { createStore } from 'redux';
import reducer from "../Reducer/Reducer";
import storeSynchronize from "redux-localstore";

export const store = createStore(reducer)
storeSynchronize(store);

