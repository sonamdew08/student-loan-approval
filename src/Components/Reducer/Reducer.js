import { defineState } from 'redux-localstore';
import { getState } from 'redux-localstore';

let defaultState = {
    jwtToken: getState().jwtToken || ""
}

const initialState = defineState(defaultState)('reducer');
export default (state = initialState, action) => {
    switch (action.type) {
        case "store-token":
            return {
                ...state,
                jwtToken: action.token
            };
        case "remove-token":
            return {
                ...state,
                jwtToken: action.token
            }
        default:
          return state;
    }
};