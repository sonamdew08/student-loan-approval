import React from 'react';

class Footer extends React.Component {
    render(){
        return (
            <div className='footer'>
                <p>2019 VARTHANA | THIRUMENI FINANCE PRIVATE LIMITED | ALL RIGHTS RESERVE</p>
            </div>
        );
    }
}

export default Footer;