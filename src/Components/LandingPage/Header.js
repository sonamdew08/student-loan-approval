import React from "react";
import '../../App.css';

class Header extends React.Component{
    render(){
        return (
            <div className='header-div'>
                <div>
                    <img 
                        width="140vw" 
                        height="140vh" 
                        src="https://varthana.com/wp-content/uploads/2018/11/cropped-Varthana-Logo.png" 
                        className="custom-logo" 
                        alt="Varthana" 
                        sizes="(max-width: 150px) 100vw, 150vh" />
                </div>
                {this.props.children}
            </div>
        );
    }
}

export default Header;