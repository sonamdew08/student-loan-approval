import React from "react";
import Navbar from "./Navbar"
import Header from "./Header"

class LandingPage extends React.Component{
    render(){
        return (
            <Header>
                <Navbar />
            </Header>
        );
    }
}

export default LandingPage;