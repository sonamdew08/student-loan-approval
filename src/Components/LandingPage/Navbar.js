import React from "react";

class Navbar extends React.Component{
    render(){
        return (
            <div className='navbar'>
                <ul className='navbar-ul'>
                    <li><a href="/signup">SignUp</a></li>
                    <li><a href='/login'>Login</a></li>
                </ul>
            </div>
        );
    }
}

export default Navbar;