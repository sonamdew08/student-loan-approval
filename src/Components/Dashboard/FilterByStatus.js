import React from "react";
import { Dropdown, DropdownItem, DropdownMenu, DropdownToggle, Input} from "reactstrap";

class FilterByStatus extends React.Component{
    constructor(props) {
        super(props);
    
        this.toggle = this.toggle.bind(this);
        this.state = {
          dropdownOpen: false
        };
    }
    
    toggle() {
        this.setState(prevState => ({
          dropdownOpen: !prevState.dropdownOpen
        }));
    }

    render(){
        return (
            <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                <DropdownToggle caret>Status</DropdownToggle>
                <DropdownMenu>
                    <DropdownItem><Input type="checkbox"/>Active</DropdownItem>
                    <DropdownItem><Input type="checkbox"/>Inprocess</DropdownItem>
                </DropdownMenu>
            </Dropdown>
        );
    }
}

export default FilterByStatus;