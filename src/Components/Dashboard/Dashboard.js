import React from 'react';
import { Table } from 'reactstrap';
import Header from '../LandingPage/Header';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { store } from '../Store/Store';
import {removeJWTToken} from '../Action/Action';
import { connect } from 'react-redux';
import ReactDataGrid from "react-data-grid";
import Unauthenticate from "../UnAuthenticate/unauthenticate";

const columns = [
    {
        key: "name",
        name: "Name"
    },
    {
        key: "phone",
        name: "Phone"
    },
    {
        key: "loan_amount",
        name: "Loan Amount"
    },
    {
        key: "loan_status",
        name: "Status"
    },
]

class Dashboard extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            studentInfo: [],
            fetch: false,
            user_id: ""
        }
    }

    componentDidMount(){
        axios.get('http://localhost:3001/userDetail')
        .then(response => {
            this.setState({studentInfo: response.data.rows, fetch: true}, () => {
                console.log("studentInfo", this.state.studentInfo)
            })
        })
    }

    handleLogout = () => {
        store.dispatch(removeJWTToken());
    }

    render(){
        if(this.props.token){
            return (
                <div className='dashboard'>
                    <Header>
                        <div className='navbar'>
                            {/* <a className="float-left"></a> */}
                            <a href='/login' className="btn btn-success float-right" onClick={this.handleLogout}>Logout</a>
                        </div>
                    </Header>
                    {this.state.fetch && 
                       
                    <Table className='dashboard-table'>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>loan Amount</th>
                                <th className="status">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.studentInfo.map((student, index) => (
                                <tr key={index}>
                                    <td>{student.name}</td>
                                    <td>{student.phone}</td>
                                    <td>{student.loan_amount}</td>
                                    <td className="status">
                                        <Link to={{ pathname:'/student', state:{user_id:student.user_id}}}>
                                            {student.loan_status}
                                        </Link>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                }                
                </div>
            );
        }
        else{
            return (
                <Unauthenticate />
            );
        }
        
    }
}

function mapStateToProps(state){
    return { token: state.jwtToken }
}

export default connect(mapStateToProps)(Dashboard);