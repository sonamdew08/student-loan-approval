import React from "react";
import { Button, Form, FormGroup, 
        Label, Input, FormFeedback, 
        Col, Container, Row, 
        Card, CardHeader, CardBody } from 'reactstrap';
import axios from 'axios';
import Header from '../LandingPage/Header';
import Navbar from '../LandingPage/Navbar';
import validateInputs from "../validation/validation";

class Register extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            name: "",
            email: "",
            password: "",
            confirmPassword: "",
            date: "",
            errors : {}
        }
        this.handleSignUpInputs = this.handleSignUpInputs.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSignUpInputs(event){
        this.setState({ [event.target.name]: event.target.value })
    }

    handleSubmit(event){
        event.preventDefault();
        const { name, email, password, confirmPassword } = this.state;
        const userDetail = {
            name: name,
            email: email,
            password: password
        }
        const error = validateInputs(name, email, password, confirmPassword)
        if(Object.keys(error).length !== 0){
            this.setState({ errors: error }, () => console.log("error state set", this.state.errors))
        }
        else{
            console.log("register part", userDetail)
            axios.post('http://localhost:8080/employee', userDetail)
            
            .then(response => {
                console.log(response)
                this.props.history.push('/login')
            })
        }
    }

    render(){
        const {errors} = this.state
        return (
            <div>
                <Header>
                    <Navbar />
                </Header>
                <Container className='login-form'>
                    <Row>
                        <Col md="4">
                            <Card className='d-flex justify-content-center login-card'>
                                <h3 className='p-3'>Welcome</h3>
                                <img src='https://image3.mouthshut.com/images/imagesp/925843871s.jpg' width="100" height="50" />
                                <CardBody>
                                    <Form onSubmit={this.handleSubmit}>
                                        <FormGroup>
                                            <Input 
                                                type="text" 
                                                name='name' 
                                                id='name' 
                                                placeholder='Enter Name' 
                                                onChange={this.handleSignUpInputs}
                                                invalid = {errors.name ? true : false} />
                                            { errors.name && <FormFeedback>{errors.name}</FormFeedback>}
                                        </FormGroup>
                                        <FormGroup>
                                            <Input 
                                                type="email" 
                                                name='email' 
                                                placeholder='Enter email address'
                                                onChange={this.handleSignUpInputs}
                                                invalid = {errors.email ? true : false} />
                                            { errors.email && <FormFeedback>{errors.email}</FormFeedback>}
                                        </FormGroup>
                                        <FormGroup>
                                            <Input 
                                                type="password" 
                                                name='password' 
                                                placeholder='Enter password' 
                                                onChange={this.handleSignUpInputs}
                                                invalid = {errors.password ? true : false}
                                                />
                                            { errors.password && <FormFeedback>{errors.password}</FormFeedback>}
                                        </FormGroup>
                                        <FormGroup>
                                            <Input 
                                                type="password" 
                                                name='confirmPassword' 
                                                placeholder='Confirm Password' 
                                                onChange={this.handleSignUpInputs}
                                                invalid = {errors.confirmPassword ? true : false}
                                                />
                                            { errors.confirmPassword && <FormFeedback>{errors.confirmPassword}</FormFeedback>}
                                        </FormGroup>
                                        <Col>
                                            <button type='submit' className='rounded-btn btn-success' >SignUp</button>
                                        </Col>
                                    </Form>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
        );

    }
}

export default Register;