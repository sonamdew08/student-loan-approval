function setJWTToken (token) {
    return {
       type: 'store-token',
       token: token 
    }
}

function removeJWTToken(){
    return {
        type: 'remove-token',
        token: ""
    }
}

export { setJWTToken,removeJWTToken };