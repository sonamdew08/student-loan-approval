import React from "react";
import { Button, Form, FormGroup, 
        Label, Input, Col, 
        Container, Row, Card, 
        CardTitle, CardBody} from 'reactstrap';
import Header from '../LandingPage/Header';
import axios from 'axios';
import Navbar from '../LandingPage/Navbar';
import Footer from '../LandingPage/Footer';
import {setJWTToken} from '../Action/Action';
import { store } from "../Store/Store";
import { connect } from 'react-redux';

class Login extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            email: "",
            password: "",
            error: ""
        }
        this.handleLogin = this.handleLogin.bind(this);
        this.handleLoginInputs = this.handleLoginInputs.bind(this);
    }
    
    handleLoginInputs(event){
        this.setState({ [event.target.name]: event.target.value })
    }

    handleLogin(event){
        event.preventDefault();
        const { email, password } = this.state;
        const userDetail = {
            email: email,
            password: password
        }
        axios.post('http://localhost:3001/login', userDetail)
            .then(response => {
                console.log(response.data)
                if(response.data.auth === true){
                    store.dispatch(setJWTToken(response.data.token))
                    this.props.history.push('/dashboard')
                }
                else{
                    this.setState({error: "Invalid email id or password"})
                }
        })
    }
   
    render(){
        return (
            <div>
                <Header>
                    <Navbar />
                </Header>
                <Container className='login-form'>
                    <Row>
                        <Col md="4">
                            <Card className='d-flex justify-content-center'>                             
                                <h3 className='p-3'>Welcome</h3>
                                <img src='https://image3.mouthshut.com/images/imagesp/925843871s.jpg' width="100" height="50" />
                                <CardBody>
                                    <Form onSubmit={this.handleLogin} className='login-page-form mt-5'>
                                        <FormGroup>
                                            <Input className="border-bottom" type="email" name="email" id="email" placeholder="Enter Email" onChange={this.handleLoginInputs} />
                                        </FormGroup>
                                        <FormGroup>
                                            <Input type="password" name="password" id="password" placeholder="enter password" onChange={this.handleLoginInputs} />
                                        </FormGroup>
                                        {this.state.error && <p style={{color: 'red'}}>{this.state.error}</p>}
                                        <Col>
                                            <button type='submit' className='rounded-btn btn-success' >Login</button>
                                        </Col>
                                    </Form>
                                </CardBody>
                            </Card>
                        </Col>
                        
                    </Row>                    
                </Container>
            </div>
        );
    }
}

function mapStateToProps(state){
    return { token: state.jwtToken }
}

export default connect(mapStateToProps)(Login);