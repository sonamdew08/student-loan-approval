import React from 'react';
import './App.css';
import LandingPage from "./Components/LandingPage/LandingPage";
import Login from "./Components/Login/Login";
import Register from "./Components/Register/Register";
import Dashboard from "./Components/Dashboard/Dashboard";
import StudentInfo from './Components/StudentInfo/StudentInfo';
import { BrowserRouter as Router, Route } from "react-router-dom";

class App extends React.Component{
    render(){
        return (
            <Router>
                <Route exact path='/' component={LandingPage} />
                <Route path='/login' component={Login} />
                <Route path='/signup' component={Register} />
                <Route path='/student' component={StudentInfo} />
                <Route path='/dashboard' component={Dashboard}/>                
            </Router>
        );
    }
}

export default App;
